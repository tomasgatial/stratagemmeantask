var request = require('supertest');  
var mongoose = require('mongoose');
var should = require('should');

var assert = require('assert');

describe('Handlers', function() {

	//running from  heroku toolbelt
	var server = request.agent("http://localhost:5000");

	var scatterGraph = {
				title : "scatterGraph",
				data_min : 10, 
				data_max : 100, 
				type: "scatter"
			}

	describe('Creating graph', function () {
		it('should create a new bar chart with data - 1 series', function (done) {
		      	
			var newGraph1 = {
				title : "newGraph1",
				data_min : 10, 
				data_max : 100, 
				type: "bar"
			}

		  	server.post("/graph/save")
		  		  .send(newGraph1)
			      .expect("Content-type",/json/)
			      .expect(200) // THis is HTTP response
			      .end(function(err,res){
				      res.body.should.have.property('_id');
				      res.body.should.have.property('data');
				      res.body.data.length.should.equal(1);
				      res.status.should.equal(200);
				      done();
				  });
		});

		it('should create a new scatter chart with data - 2 series', function (done) {

		  	server.post("/graph/save")
		  		  .send(scatterGraph)
			      .expect("Content-type",/json/)
			      .expect(200) // THis is HTTP response
			      .end(function(err,res){
				      res.body.should.have.property('_id');
				      res.body.should.have.property('data');
				      res.body.data.length.should.equal(2);
				      res.status.should.equal(200);

				      scatterGraph["_id"] = res.body["_id"];

				      done();
				  });
		});

		it('should create a new line chart with data - 1 series', function (done) {
		      	
			var newGraph3 = {
				title : "newGraph3",
				data_min : 10, 
				data_max : 100, 
				type: "line"
			}

		  	server.post("/graph/save")
		  		  .send(newGraph3)
			      .expect("Content-type",/json/)
			      .expect(200) // THis is HTTP response
			      .end(function(err,res){
				      res.body.should.have.property('_id');
				      res.body.should.have.property('data');
				      res.body.data.length.should.equal(1);
				      res.status.should.equal(200);
				      done();
				  });
		});
    });

	//
	//below should be conditional to success of above

	describe('Getting graphs', function () {

		it('should retreive previously created scatterGraph', function (done) {

			server.get("/graph/get/"+scatterGraph["_id"])
			     .expect("Content-type",/json/)
				 .expect(200) 
				 .end(function(err,res){
					 res.body.should.have.property('_id');
					 res.body.should.have.property('data');
					 res.body.data.length.should.equal(2);
					 res.body.title.should.equal(scatterGraph.title);
					 res.status.should.equal(200);
					 done();
			     });
		});

	});

	describe('Listing all graphs', function () {
		it('should list more than 3 graphs', function (done) {
		      	
		  	server.get("/graph/get")
			      .expect("Content-type",/json/)
			      .expect(200) // THis is HTTP response
			      .end(function(err,res){
				      // HTTP status should be 200
				      res.status.should.equal(200);

				      res.body.length.should.be.above(3);

				      done();
				  });
		});
    });
});