var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
//mongoose.connect('mongodb://localhost/stratagemMEANTask');
console.log(process.env.MONGOLAB_URI);
mongoose.connect(process.env.MONGOLAB_URI);

var graphSchema = mongoose.Schema({
    title: String,
    data_min: Number,
    data_max: Number,
    type: String,
    data: [{ series : [  ]}]
});

graphSchema.methods.generateData = function(){
	console.log("generate data for " + this.type);
	var numberOfSeries = this.type == "scatter" ? 2 : 1
    var newData = [];
    for (var j = 0; j < numberOfSeries; j++) {
    	var newSeries = [];

        for (var i = 0; i < 20; i++) {
        	var date = new Date(); 
        	date.setDate(date.getDate() - i);
        	var value = Math.random()*(this.data_max-this.data_min) + this.data_min;
        	newSeries.push([date.toJSON().substring(0,10) , value])
        };
        console.log("series generate");
        newData.push({series:newSeries});
    };
    this.data = newData;
};

var Graph = mongoose.model('Graph', graphSchema);

router.get('/get', function(req, res, next) {
   var query = Graph.find({})
   query.select('_id title')
   query.exec(function (err, graphs) {
        res.send(graphs);
  });
});

router.get('/get/:id', function(req, res, next) {
 	Graph.findOne({_id : req.params.id}, function(err, item){
        if(err) {
            res.send(err);
        } else if (item == null) {
            res.sendStatus(404);
        } else {
            console.log(item);
            res.send(item);
        }
    });
});

router.post('/save', function(req, res, next) {
 	console.log(req.body);

 	newGraph = new Graph(req.body);
 	newGraph.generateData();

  	newGraph.save(function (err) {
	    if (!err) {
	        return console.log("created");
	    } else {
	        return console.log(err);
	    }
	});
	res.send(newGraph);
});

module.exports = router;
