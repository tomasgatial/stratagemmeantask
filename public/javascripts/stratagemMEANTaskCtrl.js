app.controller("stratagemMEANTaskCtrl", function($scope, $http) {

    var refreshGraphList = function (){
    	$http.get("graph/get").success(function(data, status, headers, config){
		 	console.log(data)

		 	$scope.graphList = data;
		}).
		error(function(data, status, headers, config) {
	     	console.log("there was a problem");
	   });
    }

    var displayGraph = function (graphData){
    	var transformedSeries = [];
		 	graphData.data.forEach(function(d){
		 		transformedSeries.push({
		 			data : d.series
		 		})
		 	});

		 	$scope.chartConfig = {
		        options: { chart: {type: graphData.type} },
		        series: transformedSeries,
		        title: {  text: graphData.title},
		        loading: false
		    }
    }

    $scope.save  = function(newGraph) {
    	console.log(newGraph);
    	if (newGraph == null ||
    		newGraph.title == null ||
    		newGraph.data_min == null ||
    		newGraph.data_max == null ||
    		newGraph.type == null) {
    		alert("Incomplete form");
    		return;
    	};

    	if (newGraph.data_min >= newGraph.data_max) {
    		alert("Minimum is greater than maximum");
    		return;
    	};

   		$http.post("graph/save", newGraph).
   			success(function(data, status, headers, config){
   			 	console.log("added successfuly")
   			 	refreshGraphList();
   			 	displayGraph(data);
   			}).
   			error(function(data, status, headers, config) {
		     	console.log("there was a problem")
		   });
    };

    $scope.display  = function(graphToDisplay) {
    	//fetch the data
    	$http.get("graph/get/"+graphToDisplay).success(function(fetchedGraph, status, headers, config){
		 	console.log(fetchedGraph);
		 	displayGraph(fetchedGraph);
		}).
		error(function(data, status, headers, config) {
	     	console.log("there was a problem");
	   });
    }

    refreshGraphList();
});